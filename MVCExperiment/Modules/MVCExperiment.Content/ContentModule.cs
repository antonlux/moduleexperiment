﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MVCExperiment.Content.Core;
using MVCExperiment.Extensions;
using MVCExperiment.Models;
using MVCExperiment.Services;

namespace MVCExperiment.Content
{
    public class ContentModule : IModule
    {

        public ContentModule()
        {
            Features = new List<FeatureInfo>
                {
                    new FeatureInfo
                        {
                            Id="Blog",
                            Description = "Blogging system",
                            Enabled = true
                        },
                    new FeatureInfo
                        {
                            Id="Event",
                            Description = "Event system",
                            Enabled = true
                        }
                };

            Permissions = new List<Permission>()
                {
                    new Permission
                        {
                            Name = "Blog",
                            Description = "Managing blog"
                        },
                    new Permission
                        {
                            Name = "Event",
                            Description = "Managing event"
                        },
                    new Permission
                        {
                            Name ="Publishing",
                            Description = "Publish content item"
                        }
                };
        }

        public IList<string> Dependencies
        {
            get { return null; }
        }

        public IList<FeatureInfo> Features { get; private set; }

        public ModuleConfig GetConfiguration(Services.EnvironmentMode env)
        {
            switch (env)
            {
                case EnvironmentMode.Admin:
                    return new ModuleConfig
                        {
                            Installer = new ContentAdminInstaller(),
                            RouteProvider = new ContentAdminRouteProvider()
                        };

                default:
                    return null;
            }
        }

        public string Id
        {
            get { return "Content"; }
        }

        public string Description
        {
            get
            {
                return "Content module";
            }
        }


        public Data.IMongoClassMap ClassMap
        {
            get { return new ContentClassMap(); }
        }


        public IList<MVCExperiment.Models.Permission> Permissions { get; private set; }
      
    }
}
