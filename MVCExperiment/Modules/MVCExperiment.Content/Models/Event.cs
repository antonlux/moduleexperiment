﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MVCExperiment.Models;
using MongoDB.Bson.Serialization.Attributes;

namespace MVCExperiment.Content.Models
{
    [BsonDiscriminator("BlogPost")]
    public class Event : ContentItem
    {
        public override string ContentType
        {
            get
            {
                return "Event";
            }
        }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
