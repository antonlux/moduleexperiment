﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using MVCExperiment.Services;

namespace MVCExperiment.Content.Core
{
    public class ContentAdminRouteProvider :IRouteProvider
    {
        public void Configure(System.Web.Routing.RouteCollection routes)
        {
            routes.MapRoute("Blogs/{action}/{id}",
                            "blogs/{action}/{id}",
                            new
                            {
                                area = "MVCExperiment.Content",
                                controller = "BlogAdmin",
                                id = UrlParameter.Optional,
                                action = "Index"
                            },
                            new[]
                                {
                                    "MVCExperiment.Content.Controllers"
                                }
                );

            routes.MapRoute("events/{action}/{id}",
                            "events/{action}/{id}",
                            new
                            {
                                area = "Content",
                                controller = "EventAdmin",
                                id = UrlParameter.Optional
                            },
                            new[]
                                {
                                    "MVCExperiment.Content.Controllers"
                                });
        }
    }
}
