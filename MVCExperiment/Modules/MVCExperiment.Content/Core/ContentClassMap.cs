﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MVCExperiment.Content.Models;
using MVCExperiment.Data;
using MVCExperiment.Models;
using MongoDB.Bson.Serialization;

namespace MVCExperiment.Content.Core
{
    public class ContentClassMap : IMongoClassMap
    {
        public void Initialize()
        {
            BsonClassMap.RegisterClassMap<ContentItem>();
            BsonClassMap.RegisterClassMap<BlogPost>();
            BsonClassMap.RegisterClassMap<Event>();
        }
    }
}
