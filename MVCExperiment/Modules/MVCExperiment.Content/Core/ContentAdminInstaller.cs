﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using Autofac;
using MVCExperiment.Content.Controllers;

namespace MVCExperiment.Content.Core
{
    public class ContentAdminInstaller : Module 
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BlogAdminController>();
        } 
    }
}
