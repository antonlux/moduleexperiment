﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using MVCExperiment.Content.Models;
using MVCExperiment.Data;
using MVCExperiment.Mvc;

namespace MVCExperiment.Content.Controllers
{
    public class BlogAdminController : AdminController<BlogPost>
    {
        private readonly IMongoService mongoService;

        public BlogAdminController(IMongoService mongoService) : base(mongoService)
        {
            this.mongoService = mongoService;
        }
    }
}
