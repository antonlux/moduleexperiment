﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCExperiment.Controllers
{
    public class HomeController : Controller
    {
        private readonly IModuleManager moduleManager;

        public HomeController(IModuleManager moduleManager)
        {
            this.moduleManager = moduleManager;
        }

        public ActionResult Index()
        {
            var modules = moduleManager.GetActiveEntries().Select(c => c.Module).ToList();
            return View(modules);
        }
      
    }
}
