﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using MVCExperiment.Core;
using MVCExperiment.Extensions;

namespace MVCExperiment
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private IContainer container;

        protected void Application_Start()
        {
            //AreaRegistration.RegisterAllAreas();

            SetupCMS();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //HostingEnvironment.RegisterVirtualPathProvider(new ModuleResourceProvider());
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        private void SetupCMS()
        {
            var builder = new ContainerBuilder();
            var valuesProvider = new AdminEnvironmentValuesProvider();
            var moduleManager = new ModuleManager(valuesProvider);
            var adminInstaller = new AdminInstaller { ValuesProvider = valuesProvider, ModuleManager = moduleManager };
            builder.RegisterModule(new CoreInstaller());
            builder.RegisterModule(adminInstaller);
           

            moduleManager.Register(new ModuleEntry
            {
                AssemblyPath = "MVCExperiment.Content.dll",
                Id = "Content",
                IsActive = true,
                ModuleType = "MVCExperiment.Content.ContentModule"
            });
            moduleManager.ActivateRegistrations(builder);
            container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));    
        }

    }
}