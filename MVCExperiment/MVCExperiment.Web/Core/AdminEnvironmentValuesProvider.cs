﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Routing;

using MVCExperiment.Services;

namespace MVCExperiment.Core
{
    public class AdminEnvironmentValuesProvider : IEnvironmentValuesProvider
    {
        public EnvironmentMode Mode
        {
            get { return EnvironmentMode.Admin; }
        }

        public System.Web.Routing.RouteCollection Routes
        {
            get { return RouteTable.Routes;; }
        }

        public IRouteProvider DefaultRouteProvider
        {
            get { return new AdminRouteProvider(); }
        }

        public System.Web.Mvc.ViewEngineCollection ViewEngines
        {
            get {  return  System.Web.Mvc.ViewEngines.Engines; }
        }

        public string ModulesPath
        {
            get { return ConfigurationManager.AppSettings["ModulesPath"]; }
        }

        public string MongoConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["MongoDb"].ConnectionString; }
        }

        public string MongoDatabase
        {
            get { return ConfigurationManager.AppSettings["MongoDatabase"] ; }
        }
    }
}