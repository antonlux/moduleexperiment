﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Autofac.Integration.Mvc;
using MVCExperiment.Services;

namespace MVCExperiment.Core
{
    public class AdminInstaller : Module
    {
        public IEnvironmentValuesProvider ValuesProvider { get; set; }

        public IModuleManager ModuleManager { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(ValuesProvider).As<IEnvironmentValuesProvider>();
            builder.RegisterInstance(ModuleManager).As<IModuleManager>();
            builder.RegisterControllers(this.GetType().Assembly);
        }
    }
}