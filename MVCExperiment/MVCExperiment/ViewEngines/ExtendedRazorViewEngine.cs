﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using MVCExperiment.Services;

namespace MVCExperiment.ViewEngines
{
    public class ExtendedRazorViewEngine : BuildManagerViewEngine
    {
        private string[] moduleViewPaths = new string[]
            {
                @"{2}\Views\{1}\{0}.cshtml",
                @"{2}\Views\Shared\{0}.cshtml"
            };

          public ExtendedRazorViewEngine()
            : this(null) {
        }

        public ExtendedRazorViewEngine(IViewPageActivator viewPageActivator)
            : base(viewPageActivator) {
            AreaViewLocationFormats = new[] {
                "~/Areas/{2}/Views/{1}/{0}.cshtml",
                "~/Areas/{2}/Views/Shared/{0}.cshtml",
                "~/Areas/{2}/Views/Shared/{0}.vbhtml"
            };
            AreaMasterLocationFormats = new[] {
                "~/Areas/{2}/Views/{1}/{0}.cshtml",
                "~/Areas/{2}/Views/Shared/{0}.cshtml"
            };
            AreaPartialViewLocationFormats = new[] {
                "~/Areas/{2}/Views/{1}/{0}.cshtml",
                "~/Areas/{2}/Views/Shared/{0}.cshtml"
            };

            ViewLocationFormats = new[] {
                "~/Views/{1}/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml"
            };
            MasterLocationFormats = new[] {
                "~/Views/{1}/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml"
            };
            PartialViewLocationFormats = new[] {
                "~/Views/{1}/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml"
            };

            FileExtensions = new[] {
                "cshtml"
            };
        }

        #region view cache mechanism

        private static IDictionary<string, string> viewLocationCache; 

        static ExtendedRazorViewEngine()
        {
            viewLocationCache = new Dictionary<string, string>();
        }

        static string GetViewLocationFromCache(ControllerContext controllerContext, string viewName)
        {
            string controllerName = controllerContext.RouteData.GetRequiredString("controller");
            string areaName = controllerContext.RouteData.GetRequiredString("area");
            string composedPath = string.Format("{0}-{1}-{2}", areaName, controllerName, viewName);

            return viewLocationCache.ContainsKey(composedPath) ? viewLocationCache[composedPath] : string.Empty;
        }

        static void SetViewLocationCache(ControllerContext controllerContext, string viewName, string location)
        {
            string controllerName = controllerContext.RouteData.GetRequiredString("controller");
            string areaName = controllerContext.RouteData.GetRequiredString("area");
            string composedPath = string.Format("{0}-{1}-{2}", areaName, controllerName, viewName);

            viewLocationCache[composedPath] = location;
        }

        #endregion

        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            var result = base.FindView(controllerContext, viewName, masterName, useCache);

            if (result.View == null)
            {
                object area;
                controllerContext.RouteData.Values.TryGetValue("area", out area);
                
                if (area != null)
                {
                    string viewPath = string.Empty;
                    string masterPath = string.Empty;

                    if (useCache)
                    {
                        viewPath = GetViewLocationFromCache(controllerContext, viewName);
                        masterPath = GetViewLocationFromCache(controllerContext, masterName);
                    }

                    if (viewPath == string.Empty)
                    {
                        viewPath = GetPathInList(controllerContext, viewName);
                        SetViewLocationCache(controllerContext,viewName, viewPath);

                        if (masterPath == string.Empty)
                        {
                            masterPath = GetPathInList(controllerContext, masterName);
                            SetViewLocationCache(controllerContext, masterName, masterPath);
                        }
                    }

                    if (! string.IsNullOrWhiteSpace(viewPath))
                    {
                        result = new ViewEngineResult(CreateView(controllerContext,viewPath, masterPath),this);
                    }
                }
            }

            return result;
        }

        public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
        {
            if (controllerContext.RouteData.Values.ContainsKey("_ViewPath"))
                partialViewName = controllerContext.RouteData.Values["_ViewPath"].ToString();

            var result = base.FindPartialView(controllerContext, partialViewName, useCache);

            if (result.View == null)
            {
                object area;
                controllerContext.RouteData.Values.TryGetValue("area", out area);
    
                if (area != null)
                {
                    string partialPath = string.Empty;

                    if (useCache)
                        partialPath = GetViewLocationFromCache(controllerContext, partialViewName);

                    if (partialPath == string.Empty)
                    {
                        partialPath = GetPathInList(controllerContext, partialViewName);
                        SetViewLocationCache(controllerContext,partialViewName, partialPath);
                    }

                    if(! string.IsNullOrWhiteSpace(partialPath))
                        result = new ViewEngineResult(CreatePartialView(controllerContext, partialPath), this);
                }
            }

            return result;
        }

        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            return new RazorView(controllerContext, partialPath, layoutPath:null, runViewStartPages:false,
                 viewStartFileExtensions: FileExtensions, viewPageActivator: ViewPageActivator);
        }

        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            return new RazorView(controllerContext, viewPath,
                                     layoutPath: masterPath, runViewStartPages: true, viewStartFileExtensions: FileExtensions, viewPageActivator: ViewPageActivator);
        }

        private string GetPathInList(ControllerContext controllerContext, string viewName)
        {
            var valuesProvider = DependencyResolver.Current.GetService<IEnvironmentValuesProvider>();
            string controllerName = controllerContext.RouteData.GetRequiredString("controller");
            string areaName = controllerContext.RouteData.GetRequiredString("area");

            foreach (string modulePath in moduleViewPaths)
            {
                string formattedPath = string.Format(modulePath,viewName,controllerName, areaName); 
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, valuesProvider.ModulesPath, formattedPath);
                path = Path.GetFullPath(path);

                if (File.Exists(path))
                    return path;
            }
            return null;
        }
    }
}
