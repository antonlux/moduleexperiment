﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using MVCExperiment.Services;

namespace MVCExperiment.ViewEngines
{
    public class ViewEngineProvider
    {
        public IViewEngine Create(IEnvironmentValuesProvider envValues)
        {
            var areaViewFormats = new[] {
                                        "~/Views/{2}/{1}/{0}.cshtml",
                                        "~/Views/{2}/Shared/{0}.cshtml",
                                        envValues.ModulesPath+"/{2}/Views/{1}/{0}.cshtml",
                                        envValues.ModulesPath+"/{2}/Views/Shared/{0}.cshtml",
                                        "~/Themes/{2}/Views/{1}/{0}.cshtml",
                                         "~/Themes/{2}/Views/Shared/{0}.cshtml"
                                    };

            return new ExtendedRazorViewEngine
            {
                AreaMasterLocationFormats = areaViewFormats,
                AreaViewLocationFormats = areaViewFormats,
                AreaPartialViewLocationFormats = areaViewFormats
            };
        }
    }
}
