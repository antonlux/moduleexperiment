﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using MVCExperiment.Extensions;
using MVCExperiment.Models;

namespace MVCExperiment
{
    public interface IModuleManager
    {
        ICollection<ModuleEntry> GetAllEntries();

        ICollection<ModuleEntry> GetActiveEntries();

        ICollection<Permission> GetAvailablePermissions();
            
        IModule GetModule(string moduleId);

        void Register(ModuleEntry entry);

        void ActivateRegistrations(ContainerBuilder builder);

        void SetFeatureEnabled(string featureId, bool isEnabled);

        bool IsFeatureEnabled(string featureId);
    }
}
