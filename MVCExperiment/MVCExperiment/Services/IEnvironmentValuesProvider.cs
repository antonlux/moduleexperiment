﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.Mvc;
using System.Web.Routing;

namespace MVCExperiment.Services
{
    public interface IEnvironmentValuesProvider
    {
        EnvironmentMode Mode { get; }

        RouteCollection Routes { get; }

        IRouteProvider DefaultRouteProvider { get; }

        ViewEngineCollection ViewEngines { get; }

        string ModulesPath { get; }

        string MongoConnectionString { get; }

        string MongoDatabase { get; }
    }

    public enum EnvironmentMode
    {
        Admin,
        Web
    }
}
