﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MVCExperiment.Extensions;
using MVCExperiment.Models;

namespace MVCExperiment.Services
{
    public interface IContentTypeManager
    {

        void RegisterDependency(params ContentDependencyItem[] dependencies);

        IEnumerable<ContentDependencyItem> GetDependencies<T>();
    }

    public class ContentTypeDependencyManager : IContentTypeManager
    {
        private readonly IDictionary<Type, IList<ContentDependencyItem>> registries;
        private readonly IDictionary<Type, IList<ContentDependencyItem>> dependencies; 

        public ContentTypeDependencyManager()
        {
            registries = new Dictionary<Type, IList<ContentDependencyItem>>();
            dependencies = new Dictionary<Type, IList<ContentDependencyItem>>();
        }

        public void RegisterDependency(params ContentDependencyItem[] dependencies)
        {
            if (dependencies == null)
                return;

            foreach (var registration in dependencies)
            {
                if(! registries.ContainsKey(registration.Dependency))
                    registries.Add(registration.Dependency, new List<ContentDependencyItem>());
            
                registries[registration.Dependency].Add(registration);
            }
        }

        public IEnumerable<ContentDependencyItem> GetDependencies<T>()
        {
            Type type = typeof (T);

            if (dependencies.ContainsKey(type))
                return dependencies[type];

            var relatedTypes = new List<ContentDependencyItem>();

            foreach (var pair in registries)
            {
                if(type.IsAssignableFrom(pair.Key))
                {
                    relatedTypes.Add(pair.Value.FirstOrDefault(t => t.Dependant == type));
                }
            }

            dependencies[type] = relatedTypes;

            return relatedTypes;
        }
    }
}
