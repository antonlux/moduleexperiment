﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MVCExperiment.Models;

namespace MVCExperiment.Services
{
    public interface IViewModelFactory
    {
        ViewItem CreateView<T>(T value) where T:ContentItem;

        ViewPart CreateViewPart<TPart, TMain>(TPart partValue, TMain mainValue);
    }

    public class DefaultViewModelFactory : IViewModelFactory
    {
        private readonly IContentTypeManager dependencyManager;

        public DefaultViewModelFactory(IContentTypeManager dependencyManager)
        {
            this.dependencyManager = dependencyManager;
        }

        public ViewItem CreateView<T>(T value) where T : ContentItem
        {
            var viewItem = new ViewItem();
            viewItem.Data = value;




            return viewItem;
        }

        public ViewPart CreateViewPart<TPart, TMain>(TPart partValue, TMain mainValue)
        {
            throw new NotImplementedException();
        }
    }
}
