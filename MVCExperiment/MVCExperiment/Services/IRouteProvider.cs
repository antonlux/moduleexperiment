﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Routing;

namespace MVCExperiment.Services
{
    public interface IRouteProvider
    {
        void Configure(RouteCollection routes);
    }
}
