﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using MVCExperiment.Data;
using MongoDB.Driver;

namespace MVCExperiment.Mvc
{
    public class AdminController<T> : Controller
    {
        private IMongoService mongoService;

        protected IMongoSortBy SortBy { get; set; }

        protected int PageSize { get; set; }

        public AdminController(IMongoService mongoService)
        {
            this.mongoService = mongoService;
        }

        public virtual ActionResult Index(int page = 1)
        {
            MongoCursor<T> cursor = mongoService.For<T>().FindAll();

            if (SortBy != null)
                cursor = cursor.SetSortOrder(SortBy);

            var items =  cursor.ToPagedList(page);

            return View(items);
        }
    }
}
