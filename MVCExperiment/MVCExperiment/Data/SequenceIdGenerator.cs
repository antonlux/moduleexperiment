﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace MVCExperiment.Data
{
    public class SequenceIdGenerator
    {
        private const string CountersCollectionName = "SequenceIdCounters";
		public static long? Seed { get; set; }

        private readonly IMongoService mongoService;

        public SequenceIdGenerator(IMongoService mongoService)
        {
            this.mongoService = mongoService;
        }

        public object GenerateId(object container, object document)
        {
            var collection = (MongoCollection) container;
            var query = Query.EQ("_id", collection.Name);
            var sort = SortBy.Ascending();
            var update = Update.Inc("Next", 1);

			if (Seed != null)
			{
				// Only use seed if collection is not already tracked
				var collCounter = mongoService.Database.GetCollection(CountersCollectionName).FindOne(query);
				if (collCounter == null)
					update = Update.Set("Next", Seed.Value + 1);
			}

            var result = mongoService
                .Database.GetCollection(CountersCollectionName)
                .FindAndModify(query, sort, update, true, true)
                .GetModifiedDocumentAs<SequenceNumberCounter>();

            return result.Next;
        }

        public bool IsEmpty(object id)
        {
            return id == null || (int) id == 0;
        }

        private sealed class SequenceNumberCounter
        {
            [BsonId]
            public string CollectionName { get; set; }
            public long Next { get; set; }
        }
    }
}
