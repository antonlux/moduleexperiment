﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MVCExperiment.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace MVCExperiment.Data
{
    public static class MongoCollectionExtensions
    {
        public static HiLoIdGenerator HiLoIdGenerator = new HiLoIdGenerator(20);

        public static long GenerateSequenceId<T>(this MongoCollection<T> collection)
        {
            var sequenceIdGenerator = new SequenceIdGenerator(new MongoService());
            return (long)sequenceIdGenerator.GenerateId(collection, null);
        }

        public static long GenerateHiLoId<T>(this MongoCollection<T> collection)
        {
            return HiLoIdGenerator.GenerateId(collection.Name, collection.Database);
        }

        public static MongoCursor<T> ById<T>(this MongoCollection<T> collection, ObjectId id) where T : MongoEntity, new()
        {
            return collection.Find(Query.EQ("_id", id));
        }
    }
}
