﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MVCExperiment.Models;
using MongoDB.Driver;

namespace MVCExperiment.Data
{
    public static class MongoCursorExtensions
    {
        public static PagedList<T> ToPagedList<T>(this MongoCursor<T> cursor, int page)
        {
            int skip = (page - 1) * cursor.Limit;
            IList<T> results = cursor.SetLimit(cursor.Limit).SetSkip(skip).ToList();
            return new PagedList<T>(results, page, cursor.Limit, (int)cursor.Count());
        }
    }
}
