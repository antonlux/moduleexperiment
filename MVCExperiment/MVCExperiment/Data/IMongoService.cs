﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;

namespace MVCExperiment.Data
{
    public interface IMongoService
    {
        MongoServer Server { get; }

        MongoDatabase Database { get;  }

        MongoCollection<T> For<T>();

        MongoCollection For(Type type);

        MongoCollection For(string collectionName);
    }
}
