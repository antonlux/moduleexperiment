﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace MVCExperiment.Data
{
    /// <summary>
    /// Class that generates a new identity value using the HILO algorithm.
    /// Only one instance of this class should be used in your project
    /// </summary>
    public class HiLoIdGenerator
    {
        private readonly long capacity;
        private readonly ReaderWriterLockSlim lockSlim = new ReaderWriterLockSlim();
        private long currentHi;
        private long currentLo;

        public HiLoIdGenerator(long capacity)
        {
            currentHi = 0;
            this.capacity = capacity;
            currentLo = capacity + 1;
        }

        /// <summary>
        /// Generates a new identity value
        /// </summary>
        /// <param name="collectionName">Collection Name</param>
        /// <param name="database"></param>
        /// <returns></returns>
        public long GenerateId(string collectionName, MongoDatabase database)
        {
            lockSlim.EnterUpgradeableReadLock();
            long incrementedCurrentLow = Interlocked.Increment(ref currentLo);
            if (incrementedCurrentLow > capacity)
            {
                lockSlim.EnterWriteLock();
                if (Thread.VolatileRead(ref currentLo) > capacity)
                {
                    currentHi = GetNextHi(collectionName, database);
                    currentLo = 1;
                    incrementedCurrentLow = 1;
                }
                lockSlim.ExitWriteLock();
            }
            lockSlim.ExitUpgradeableReadLock();
            return (currentHi - 1) * capacity + (incrementedCurrentLow);
        }

        private static long GetNextHi(string collectionName, MongoDatabase database)
        {
            while (true)
            {
                try
                {
                    var query = Query.EQ("_id", collectionName);
                    var sort = SortBy.Ascending();
                    var update = Update.Inc("ServerHi", 1);
                    var result = database.GetCollection<CollectionHiLoKey>("CollectionHiLoKey").FindAndModify(query, sort, update);
                    var hiLoKey = result.GetModifiedDocumentAs<CollectionHiLoKey>();

                    if (hiLoKey == null)
                    {
                        database.GetCollection<CollectionHiLoKey>("CollectionHiLoKey").Insert(new CollectionHiLoKey { CollectionName = collectionName, ServerHi = 2 });
                        return 1;
                    }

                    var newHi = hiLoKey.ServerHi;
                    return newHi;
                }
                catch (MongoException ex)
                {
                    if (!ex.Message.Contains("duplicate key"))
                        throw;
                }
            }
        }

        #region Nested type: HiLoKey

        private class CollectionHiLoKey
        {
            [BsonId]
            public string CollectionName { get; set; }
            public long ServerHi { get; set; }
        }

        #endregion
    }

}
