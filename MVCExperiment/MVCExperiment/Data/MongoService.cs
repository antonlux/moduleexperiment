﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using MVCExperiment.Services;
using MongoDB.Driver;

namespace MVCExperiment.Data
{
    public class MongoService : IMongoService
    {
        private readonly MongoClient client;

        public MongoService() 
        {
            client = new MongoClient(ConfigurationManager.ConnectionStrings["MongoDb"].ConnectionString);
            Server = client.GetServer();
            Database = Server.GetDatabase(ConfigurationManager.AppSettings["MongoDatabase"]);
        }

        public MongoService(IEnvironmentValuesProvider valuesProvider) :
            this(valuesProvider.MongoConnectionString,valuesProvider.MongoDatabase)
        {
            
        }

        public MongoService(string connectionString, string dbName)
        {
            client = new MongoClient(connectionString);
            Server = client.GetServer();
            Database = Server.GetDatabase(dbName);
        }

        public MongoDB.Driver.MongoServer Server { get; private set; }

        public MongoDB.Driver.MongoDatabase Database { get; private set; }

        public MongoDB.Driver.MongoCollection<T> For<T>()
        {
            return Database.GetCollection<T>(typeof(T).Name);
        }

        public MongoDB.Driver.MongoCollection For(Type type)
        {
            return Database.GetCollection(type.Name);
        }

        public MongoDB.Driver.MongoCollection For(string collectionName)
        {
            return Database.GetCollection(collectionName);
        }
    }
}
