﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using MVCExperiment.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MVCExperiment.Data
{
    public interface IRepository<T> where T: MongoEntity
    {
    
        IList<T> GetAll();

        void Add(T item);

        T FindOne(ObjectId id);

        T FindOne(int id);

        T FindOne(Expression<Func<T, bool>> expression);

        void Save(T item);

        void Delete(ObjectId id);

        MongoCollection<T> Collection { get; }

        long GenerateSequenceId();

        event RepositoryItemEventHandler<T> ItemAdded;

        event RepositoryItemEventHandler<T> ItemUpdated;

        event RepositoryItemEventHandler<T> ItemSaved;
    }

    public delegate void RepositoryItemEventHandler<in T>(T item);
}
