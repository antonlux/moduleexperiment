﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using MVCExperiment.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MVCExperiment.Data
{
    public class Repository<T> : IRepository<T> where T:MongoEntity
    {
        private readonly IMongoService mongoService;

        public Repository(IMongoService mongoService)
        {
            this.mongoService = mongoService;
        }

        public MongoCollection<T> Collection
        {
            get
            {
                return mongoService.For<T>();
            }
        }

        public IList<T> GetAll()
        {
            return Collection.FindAll().ToList();
        }

        public void Add(T item)
        {
            
        }

        public T FindOne(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public T FindOne(int id)
        {
            throw new NotImplementedException();
        }

        public T FindOne(Expression<Func<T, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public void Save(T item)
        {
            throw new NotImplementedException();
        }

        public void Delete(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public long GenerateSequenceId()
        {
            return Collection.GenerateSequenceId();
        }

        public event RepositoryItemEventHandler<T> ItemAdded;

        public event RepositoryItemEventHandler<T> ItemUpdated;

        public event RepositoryItemEventHandler<T> ItemSaved;
    }
}
