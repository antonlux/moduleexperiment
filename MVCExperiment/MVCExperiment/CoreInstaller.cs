﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using MVCExperiment.Data;

namespace MVCExperiment
{
    public class CoreInstaller : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MongoService>().As<IMongoService>().SingleInstance();
            base.Load(builder);
        }
    }
}
