using System;
using System.ComponentModel.DataAnnotations;

namespace MVCExperiment.Models
{
    public class ContentItem : MongoEntity
    {
        public ContentItem()
        {
            DateTime localTime = DateTime.UtcNow;
            DatePublished = localTime;
        }

        public virtual string ContentType { get { return null; } }

        [Required]
        public virtual string Title { get; set; }

        [Required]
        public virtual string Slug { get; set; }

        [Required]
        public virtual DateTime DatePublished { get; set; }

        public virtual string Content { get; set; }

        public virtual bool IsPublished { get; set; }
    }
}
