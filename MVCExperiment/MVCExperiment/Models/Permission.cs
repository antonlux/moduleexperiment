﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVCExperiment.Models
{
    public class Permission
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public override int GetHashCode()
        {
            if (string.IsNullOrWhiteSpace(Name))
                return 0;

            return Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var permission = obj as Permission;

            if (permission == null)
                return false;
            
            return Name == permission.Name;
        }
    }
}
