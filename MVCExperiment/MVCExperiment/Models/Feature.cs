﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVCExperiment.Models
{
    public class Feature : MongoEntity
    {
        public string FeatureId { get; set; }

        public string ModuleId { get; set; }

        public bool IsEnabled { get; set; }
    }
}
