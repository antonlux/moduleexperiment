﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVCExperiment.Models
{
    public class ViewPart : IViewItem
    {
        public virtual string Id { get; set; }

        public ViewItem MainView { get; set; }

        public virtual ViewPartPosition Position 
        { 
            get
            {
                return ViewPartPosition.Bottom;
            }
        }
    }

    public enum ViewPartPosition
    {
        Top,
        Bottom
    }
}
