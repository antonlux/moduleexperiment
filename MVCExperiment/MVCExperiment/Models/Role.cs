﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace MVCExperiment.Models
{
    public class Role : MongoEntity
    {
        public Role()
        {
            
        }

        [Required]
        public string Name { get; set; }

        public IList<string> Permissions { get; set; } 
    }
}
