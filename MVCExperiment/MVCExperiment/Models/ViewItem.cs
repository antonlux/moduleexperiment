﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVCExperiment.Models
{
    public interface IViewItem
    {
        string Id { get;  }

        ViewItem MainView { get;  }
    }

    public class ViewItem : IViewItem
    {
        public ViewItem()
        {
            Parts = new List<ViewPart>();
        }

        public IList<ViewPart> Parts { get; private set; }

        public ContentItem Data { get; set; } 

        public string Id
        {
            get { return Data.Id.ToString(); }
        }

        public ViewItem MainView
        {
            get { return this; }
        }
    }

    public enum ViewType
    {
        List,
        Editor
    }
}
