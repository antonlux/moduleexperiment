﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Autofac;
using MVCExperiment.Extensions;
using MVCExperiment.Models;
using MVCExperiment.Services;
using MVCExperiment.ViewEngines;

namespace MVCExperiment
{
    public class ModuleManager : IModuleManager
    {
        private IDictionary<string, ModuleEntry> entries;
        private IDictionary<string, FeatureInfo> features;
        private IEnvironmentValuesProvider envValues;
        private IList<Permission> permissions; 

        public ModuleManager(IEnvironmentValuesProvider envValues)
        {
            entries = new Dictionary<string, ModuleEntry>();
            features = new Dictionary<string, FeatureInfo>();
            permissions = new List<Permission>();

            this.envValues = envValues;

            SetupDefaultModules();
        }

        public void Register(ModuleEntry entry)
        {
            entries.Add(entry.Id, entry);
        }

        public void Activate(ContainerBuilder builder, ModuleEntry entry)
        {
            if (entry.Module != null) return;

            Assembly assembly = null;

            if (!string.IsNullOrEmpty(entry.AssemblyPath))
            {
                assembly = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin", entry.AssemblyPath));
            }
            else
            {
                assembly = Assembly.GetAssembly(typeof(IModuleManager));
            }

            entry.Assembly = assembly;
            entry.Module = assembly.CreateInstance(entry.ModuleType) as IModule;

            if (entry.Module.Dependencies != null && entry.Module.Dependencies.Count > 0)
            {
                foreach (var dep in entry.Module.Dependencies)
                {
                    Activate(builder, entries[dep]);
                }
            }

            var config = entry.Module.GetConfiguration(envValues.Mode);
            if (config != null)
            {
                if (config.Installer != null)
                    builder.RegisterModule(config.Installer);

                if (config.RouteProvider != null)
                    config.RouteProvider.Configure(envValues.Routes);
            }

            entry.Module.ClassMap.Initialize();
            entry.IsActive = true;

            if (entry.Module.Features != null)
            {
                foreach (var feature in entry.Module.Features)
                {
                    features.Add(feature.Id, feature);
                }
            }

            if (entry.Module.Permissions != null)
            {
                foreach (var permission in entry.Module.Permissions)
                {
                    if(! permissions.Contains(permission))
                        permissions.Add(permission);
                }
            }
        }

        public void ActivateRegistrations(ContainerBuilder builder)
        {
            foreach (var entryPair in entries)
            {
                var entry = entryPair.Value;

                Activate(builder, entry);
            }

            envValues.DefaultRouteProvider.Configure(envValues.Routes);
            
            //envValues.ViewEngines.Clear();
            //envValues.ViewEngines.Add(new ExtendedRazorViewEngine());
        }

        public IModule GetModule(string moduleId)
        {
            return entries[moduleId].Module;
        }

        public ICollection<ModuleEntry> GetAllEntries()
        {
            return new ReadOnlyCollection<ModuleEntry>(entries.Select(e => e.Value).ToList());
        }

        void SetupDefaultModules()
        {

        }

        public ICollection<ModuleEntry> GetActiveEntries()
        {
            return new ReadOnlyCollection<ModuleEntry>(entries.Where(e => e.Value.IsActive).Select(e => e.Value).ToList());
        }

        public void SetFeatureEnabled(string featureId, bool isEnabled)
        {
            features[featureId].Enabled = isEnabled;
        }

        public bool IsFeatureEnabled(string featureId)
        {
            return features[featureId].Enabled;
        }

        public ICollection<Models.Permission> GetAvailablePermissions()
        {
            return new ReadOnlyCollection<Permission>(permissions);
        }
    }
}
