﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MVCExperiment.Extensions
{
    public class ModuleEntry
    {
        public string Id { get; set; }

        public string ModuleType { get; set; }

        public string AssemblyPath { get; set; }

        public IModule Module { get; set; }

        public Assembly Assembly { get; set; }

        public bool IsActive { get; set; }
    }
}
