﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVCExperiment.Extensions
{
    public class FeatureInfo
    {
        public FeatureInfo()
        {
            Dependencies = new List<string>();
        }

        public string Id { get; set; }

        public string Description { get; set; }

        public bool Enabled { get; set; }

        public IList<string> Dependencies { get; set; }  
    }
}
