﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVCExperiment.Extensions
{
    public interface IContentFilter
    {
        IDictionary<string,object> PipelineContext { get; set; }

        void OnLoad();

        void OnValidation();

        void OnCreate();

        void OnUpdate();

        void OnDelete();
    }
}
