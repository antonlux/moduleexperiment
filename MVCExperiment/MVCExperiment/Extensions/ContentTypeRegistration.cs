﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MVCExperiment.Models;
using MVCExperiment.Services;

namespace MVCExperiment.Extensions
{
    public abstract class ContentTypeRegistration
    {


        public abstract void RegisterDependency(IContentTypeManager manager);
    }

    public class ContentRegistrationItem
    {
        public ContentRegistrationItem()
        {
            
        }

        public ContentRegistrationItem(Type type)
        {
            Type = type;
        }

        public Type Type { get; private set; }

        public Type Editor { get; private set; }

        public Type EditorHandler { get; private set; }

        public ContentRegistrationItem UseEditor<T>()
        {
            Editor = typeof (T);
            if(! typeof(ViewPart).IsAssignableFrom(Editor))
                throw new ArgumentException();

            return this;
        }

        public ContentRegistrationItem HandledBy<T>()
        {
            Type filterType = typeof(T);

            if (!typeof(IContentFilter).IsAssignableFrom(filterType))
                throw new ArgumentException();

            EditorHandler = filterType;
            return this;
        }
    }

    public class ContentDependencyItem
    {
        public ContentDependencyItem()
        {
            
        }

        public ContentDependencyItem(Type dependant)
        {
            Dependant = dependant;
        }

        public Type Dependant { get; private set; }

        public Type Dependency { get; private set; }

        public Type Editor { get; private set; }

        public ViewPartPosition Position { get; private set; } 

        public Type EditorHandler { get; set; }
    
        public ContentDependencyItem DependentTo<T>()
        {
            Dependency = typeof (T);
            return this;
        }

        public ContentDependencyItem UseEditor<T>()
        {
            Type editorType = typeof (T);

            if(! typeof(ViewPart).IsAssignableFrom(editorType))
                throw new ArgumentException();

            Editor = editorType;
            return this;
        }

        public ContentDependencyItem PlacedAt(ViewPartPosition position)
        {
            Position = position;
            return this;
        }

        public ContentDependencyItem HandledBy<T>()
        {
            Type filterType = typeof (T);

            if(! typeof(IContentFilter).IsAssignableFrom(filterType))
                throw new ArgumentException();

            EditorHandler = filterType;
            return this;
        }
    }

    public static class ContentRegistration
    {
        public static ContentDependencyItem DependencyFor<T>()
        {
            return new ContentDependencyItem(typeof(T));
        }

        public static ContentRegistrationItem For<T>()
        {
            return new ContentRegistrationItem(typeof(T));
        }
    }
}
