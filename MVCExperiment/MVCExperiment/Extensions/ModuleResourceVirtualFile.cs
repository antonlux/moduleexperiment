﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace MVCExperiment.Extensions
{
    public class ModuleResourceVirtualFile : VirtualFile
    {
        private string path;

        public ModuleResourceVirtualFile(string virtualPath)
            : base(virtualPath)
        {
            path = virtualPath;
        }

        public override System.IO.Stream Open()
        {
            if (!Path.IsPathRooted(path) || path.StartsWith("\\") || !path.StartsWith("/"))
                path = HttpContext.Current.Server.MapPath(path);

            return new FileStream(path, FileMode.Open,FileAccess.Read,FileShare.Read);

        }
    }
}
