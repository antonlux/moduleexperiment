﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;

namespace MVCExperiment.Extensions
{
    public class ModuleResourceProvider : VirtualPathProvider
    {
      
        public override bool FileExists(string virtualPath)
        {
            return CheckFileExists(virtualPath);
        }

        public override VirtualFile GetFile(string virtualPath)
        {
            if (CheckAppResourcePath(virtualPath))
            {
                return new ModuleResourceVirtualFile(virtualPath);
            }
            else
            {
                return base.GetFile(virtualPath);
            }
        }

        public override CacheDependency GetCacheDependency(string virtualPath, IEnumerable virtualPathDependencies, DateTime utcStart)
        {
            if (CheckAppResourcePath(virtualPath))
                return null;

            return base.GetCacheDependency(virtualPath, virtualPathDependencies, utcStart);
        }

        private bool CheckAppResourcePath(string virtualPath)
        {
            return Path.IsPathRooted(virtualPath) && !virtualPath.StartsWith("\\") && !virtualPath.StartsWith("/");
        }

        private bool CheckFileExists(string virtualPath)
        {
            if (CheckAppResourcePath(virtualPath) && File.Exists(virtualPath))
            {
                return true;
            }
            else
            {
                return base.FileExists(virtualPath);
            }
        }
    }
}
