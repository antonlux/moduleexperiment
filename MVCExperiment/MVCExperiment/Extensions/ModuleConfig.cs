﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using MVCExperiment.Services;

namespace MVCExperiment.Extensions
{
    public class ModuleConfig
    {
        public IRouteProvider RouteProvider { get; set; }

        public Module Installer { get; set; }
    }
}
