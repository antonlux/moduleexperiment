﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MVCExperiment.Data;
using MVCExperiment.Models;
using MVCExperiment.Services;

namespace MVCExperiment.Extensions
{
    public interface IModule
    {
        string Id { get; }

        string Description { get;}

        IList<string> Dependencies { get; }

        IList<FeatureInfo> Features { get; }

        IList<Permission> Permissions { get; }
            
        IMongoClassMap ClassMap { get; }

        ModuleConfig GetConfiguration(EnvironmentMode env);
    }
}
